﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
[Serializable]
public struct MovementData : IComponentData
{//不允许有逻辑和对外依赖
    public float theta;
    public float3 center;//轴心
    public float omega;//角速度
    public float radius;//半径
    public float p;
}
